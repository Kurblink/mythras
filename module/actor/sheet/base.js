import { skillTypes } from '../../item/skill-helper.js'
/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class ActorSheetMythras extends ActorSheet {
  /** @override */
  static get defaultOptions() {
    return super.defaultOptions
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    const data = super.getData()
    data.dtypes = ['String', 'Number', 'Boolean']

    //Prepare items.
    if (this.actor.data.type == 'character') {
      this._prepareCharacterItems(data)
    }

    return data
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor

    // Initialize containers.
    const gear = []
    const hitLocations = []
    const standardSkills = []
    const professionalSkills = []
    const combatStyles = []
    const magicSkills = []
    const passions = []
    const skillsAndPassions = []
    const meleeWeapons = []
    const rangedWeapons = []
    const armor = []
    const equipment = []
    const currency = []
    const abilities = []

    const itemMapper = {
      hitLocation: hitLocations,
      standardSkill: standardSkills,
      professionalSkill: professionalSkills,
      combatStyle: combatStyles,
      magicSkill: magicSkills,
      passion: passions,
      'melee-weapon': meleeWeapons,
      'ranged-weapon': rangedWeapons,
      armor: armor,
      equipment: equipment,
      currency: currency,
      ability: abilities
    }

    // Iterate through items, allocating to containers
    // let totalWeight = 0;
    for (let i of this.actor.items.values()) {
      let item = i.data
      //i.img = i.img || DEFAULT_TOKEN

      itemMapper[i.type].push(i)
      if (skillTypes.includes(i.type)) {
        skillsAndPassions.push(i)
      }
    }

    // Assign and return
    actorData.gear = gear
    hitLocations.sort(function (a, b) {
      return a.data.sort - b.data.sort
    })
    actorData.hitLocations = hitLocations
    standardSkills.sort(function (a, b) {
      return a.data.name.localeCompare(b.data.name)
    })
    actorData.standardSkills = standardSkills
    professionalSkills.sort(function (a, b) {
      return a.data.name.localeCompare(b.data.name)
    })
    actorData.professionalSkills = professionalSkills
    actorData.combatStyles = combatStyles
    actorData.magicSkills = magicSkills
    actorData.passions = passions
    actorData.skillsAndPassions = skillsAndPassions
    actorData.meleeWeapons = meleeWeapons
    actorData.rangedWeapons = rangedWeapons
    actorData.armor = armor
    actorData.equipment = equipment
    actorData.currency = currency
    actorData.abilities = abilities
  }

  /* -------------------------------------------- */
  /** @override */
  _updateObject(event, formData) {
    const actor = this.getData().actor
    const skills = actor.skillsAndPassions
    const hitLocations = actor.hitLocations
    if (event.target != null) {
      if (event.target.id.includes('characteristic-box')) {
        let affectedChar = event.target.id.slice(0, 3)
        skills.forEach((skill) => {
          let primChar = Number(
            formData[
              'data.characteristics.' + skill.data.primaryChar + '.value'
            ]
          )
          let secondChar = Number(
            formData[
              'data.characteristics.' + skill.data.secondaryChar + '.value'
            ]
          )
          if (
            skill.data.primaryChar === affectedChar ||
            skill.data.secondaryChar === affectedChar
          ) {
            this.actor.updateEmbeddedEntity('OwnedItem', {
              _id: skill._id,
              'data.baseVal.value': primChar + secondChar,
              'data.totalVal':
                primChar +
                secondChar +
                Number(skill.data.trainingVal) +
                Number(skill.data.miscBonus)
            })
          }
        })
      }

      if (event.target.id.includes('_equipped')) {
        let armorInfo = event.target.id.split('_')
        let armor = this.actor.getOwnedItem(armorInfo[1])
        let equipped = formData['item.data.data.equipped']
        if (Array.isArray(equipped)) {
          equipped = equipped[armorInfo[0]]
        }
        this.actor.updateEmbeddedEntity('OwnedItem', {
          _id: armor._id,
          'data.equipped': equipped
        })
        let hitLoc = this.actor.getOwnedItem(armor.data.data.location)
        let armors = hitLoc.data.data.armors.split(',')
        let ap = hitLoc.data.data.ap
        let attached = {}
        if (Boolean(equipped)) {
          if (hitLoc.data.data.attached !== undefined) {
            attached = hitLoc.data.data.attached
          }
          attached[armor._id] = [armor.data.name, armor.data.data.ap]
          armors.push(armor.data.name)
          ap += armor.data.data.ap
        } else {
          delete attached[armor._id]
          armors = armors.filter(function (value) {
            return armor.data.name !== value
          })
          ap -= armor.data.data.ap
        }
        this.actor.updateEmbeddedEntity('OwnedItem', {
          _id: hitLoc._id,
          'data.armors': armors.join(','),
          'data.ap': ap,
          'data.attached': attached
        })
      }

      if (event.target.id.includes('_hitLoc')) {
        let fieldInfo = event.target.id.split('_')
        let hitLocIndex = fieldInfo[0]
        let hitLoc = this.actor.getOwnedItem(fieldInfo[1])
        let hitLocField = fieldInfo[2]
        let updateField = ''
        let newFieldValue = ''
        if (hitLocField === 'name') {
          updateField = 'name'
          newFieldValue = formData['item.data.name'][Number(hitLocIndex)]
        } else {
          updateField = 'data.' + hitLocField
          newFieldValue =
            formData['item.data.data.' + hitLocField][Number(hitLocIndex)]
        }
        this.actor.updateEmbeddedEntity('OwnedItem', {
          _id: hitLoc._id,
          [updateField]: newFieldValue
        })
      }
      if (
        event.target.id.includes('maxHpMod') ||
        event.target.id.includes('con_characteristic-box') ||
        event.target.id.includes('siz_characteristic-box')
      ) {
        hitLocations.forEach((hitLoc, index) => {
          let newHp =
            Number(hitLoc.data.data.baseHp) +
            Math.ceil(
              (Number(formData['data.characteristics.siz.value']) +
                Number(formData['data.characteristics.con.value'])) /
                5
            ) +
            Number(formData['data.attributes.hitPointMod.mod']) +
            Number(formData['item.data.data.maxHpMod'][index])
          if (newHp < 1) {
            newHp = 1
          }
          this.actor.updateEmbeddedEntity('OwnedItem', {
            _id: hitLoc.data._id,
            'data.maxHp': newHp,
            'data.maxHpMod': formData['item.data.data.maxHpMod'][index]
          })
        })
      }
    }

    return this.actor.update(formData)
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html)
    const actor = this.actor

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return

    // Add Actor Item
    html.find('.item-create').click(this._onItemCreate.bind(this))

    // Update Actor Item
    html.find('.item-edit').click((ev) => {
      const li = $(ev.currentTarget).parents('.item')
      const item = actor.getOwnedItem(li.data('itemId'))
      item.sheet.render(true)
    })

    // Delete Actor Item
    html.find('.item-delete').click((ev) => {
      const li = $(ev.currentTarget).parents('.item')
      actor.deleteOwnedItem(li.data('itemId'))
      li.slideUp(200, () => this.render(false))
    })

    // Skill roll button listener
    html.find('.rollableSkill').click(this._onRollSkill.bind(this))

    // Melee Weapon roll button listener
    html.find('.rollableMeleeDamage').click(this._onRollMeleeDamage.bind(this))

    // Ranged Weapon roll button listener
    html
      .find('.rollableRangedDamage')
      .click(this._onRollRangedDamage.bind(this))

    // Hit Location roll button listener
    html.find('.roll-hitlocations-button').click(this._onRollHitLoc.bind(this))

    // Actor Current Point increase listeners
    const pointIncreaseMapping = {
      '#increase-current-lp': 'currentLuckPoints',
      '#increase-current-mp': 'currentMagicPoints',
      '#increase-current-ap': 'currentActionPoints',
      '#increase-current-er': 'experienceRolls'
    }
    for (const [key, value] of Object.entries(pointIncreaseMapping)) {
      html.find(key).click(function (event) {
        event.preventDefault()
        actor.update({
          ['data.' + value]: Number(actor.data.data[value]) + 1
        })
      })
    }

    // Actor Current Point decrease listeners
    const pointDecreaseMapping = {
      '#decrease-current-lp': 'currentLuckPoints',
      '#decrease-current-mp': 'currentMagicPoints',
      '#decrease-current-ap': 'currentActionPoints',
      '#decrease-current-er': 'experienceRolls'
    }
    for (const [key, value] of Object.entries(pointDecreaseMapping)) {
      html.find(key).click(function (event) {
        event.preventDefault()
        actor.update({
          ['data.' + value]: Number(actor.data.data[value]) - 1
        })
      })
    }

    // Drag events for macros.
    if (actor.owner) {
      let handler = (ev) => this._onDragItemStart(ev)
      html.find('li.item').each((i, li) => {
        if (li.classList.contains('inventory-header')) return
        li.setAttribute('draggable', true)
        li.addEventListener('dragstart', handler, false)
      })
    }
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault()
    const header = event.currentTarget
    // Get the type of item to create.
    const type = header.dataset.type
    // Grab any data associated with this control.
    const data = duplicate(header.dataset)
    // Initialize a default name.
    var name = `New ${type.capitalize().replace(/([a-z])([A-Z])/g, '$1 $2')}`
    if (game.i18n) {
      name = game.i18n.localize(`MYTHRAS.New_${type}`)
    }
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    }
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data['type']
    // Finally, create the item!
    return this.actor.createOwnedItem(itemData)
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  _onRollSkill(event) {
    event.preventDefault()
    const element = event.currentTarget
    const dataset = element.dataset
    const dataLabel = dataset.label.split(',')
    const diffGrades = [2, 1.5, 1, 2 / 3, 0.5, 0.1].map(function (x) {
      return Math.ceil(x * Number(dataLabel[1]))
    })
    const diffNames = [
      'Very Easy: ',
      'Easy: ',
      'Standard: ',
      'Hard: ',
      'Formidable: ',
      'Herculean: '
    ]

    if (dataset.roll) {
      let roll = new Roll(dataset.roll, this.actor.data.data)
      let label = dataset.label ? `Rolling ${dataLabel[0]}` : ''
      const rolled = roll.roll()
      let diffRolled = diffNames.map(function (x) {
        return '<strong>' + x + '</strong>' + rolled.result + ' <b>≤</b> '
      })
      let contentString =
        '<h3><strong>Roll: ' + rolled.result + '</strong></h3>'
      diffRolled.forEach((rollStr, index) => {
        contentString += rollStr + diffGrades[index] + '<br>'
      })
      roll.toMessage({
        user: game.user._id,
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label,
        content: contentString
      })
    }
  }
  _onRollMeleeDamage(event) {
    event.preventDefault()
    const element = event.currentTarget
    const dataset = element.dataset
    const dataLabel = dataset.label.split(',')
    const name = dataLabel[0]
    const weaponDam = dataLabel[1]
    const damMod = dataLabel[2] === 'true'
    const combatEffect = dataLabel[3]
    const traits = dataLabel[4]
    if (dataset.roll) {
      let damage = dataset.roll
      if (damMod) {
        damage += '+' + this.actor.data.data.attributes.damageMod.value
      }
      let roll = new Roll(damage, this.actor.data.data)
      let label = dataset.label ? `Rolling ${name}` : ''
      label +=
        '<br><strong>Combat-Effects: </strong>' +
        combatEffect +
        '<br><strong>Traits: </strong>' +
        traits
      roll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label
      })
    }
  }
  _onRollRangedDamage(event) {
    event.preventDefault()
    const element = event.currentTarget
    const dataset = element.dataset
    const dataLabel = dataset.label.split(',')
    const name = dataLabel[0]
    const weaponDam = dataLabel[1]
    const damMod = dataLabel[2] === 'true'
    const combatEffect = dataLabel[3]
    if (dataset.roll) {
      let damage = dataset.roll
      if (damMod) {
        damage += '+' + this.actor.data.data.attributes.damageMod.value
      }
      let roll = new Roll(damage, this.actor.data.data)
      let label = dataset.label ? `Rolling ${name}` : ''
      label += '<br><strong>Combat-Effects: </strong>' + combatEffect
      roll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label
      })
    }
  }
  _onRollHitLoc(event) {
    event.preventDefault()
    const element = event.currentTarget
    const dataset = element.dataset
    const hitLoc = dataset.label.split(',')
    if (dataset.roll) {
      let roll = new Roll(dataset.roll, this.actor.data.data)
      const rolled = roll.roll()
      const rollResult = Number(rolled.result)
      let label = dataset.label ? `Rolling Hit Location` : ''
      const locHit = hitLoc.filter(function (value) {
        let loc = value.split('/')
        return rollResult >= Number(loc[1]) && rollResult <= Number(loc[2])
      })
      let loc = String(locHit).split('/')
      label += '<br><h2>' + loc[0] + '</h2>'
      roll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label
      })
    }
  }
}
