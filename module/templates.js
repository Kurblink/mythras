export default function () {
  const templatePaths = [
    // Global partials
    'systems/mythras/templates/global/mythras-symbols.html',

    // Actor partials
    'systems/mythras/templates/actor/tabs/actor-core.html',
    'systems/mythras/templates/actor/tabs/actor-skills.html',
    'systems/mythras/templates/actor/tabs/actor-combat.html',
    'systems/mythras/templates/actor/tabs/actor-abilities.html',
    'systems/mythras/templates/actor/tabs/actor-equipment.html',
    'systems/mythras/templates/actor/tabs/actor-notes.html'
  ]
  return loadTemplates(templatePaths)
}
